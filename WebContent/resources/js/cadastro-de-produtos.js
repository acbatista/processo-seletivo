var inicio = new Vue({
	el:"#cadastro-produto",
    data: {
    	form: {
            email: '',
            name: '',
            food: null,
            checked: []
          },
          foods: [{ text: 'Selecione', value: null }, 'Carrots', 'Beans', 'Tomatoes', 'Corn'],
          show: true
    },
    created: function(){
        let vm =  this;
        vm.buscaProdutos();
    },
    methods:{
        buscaProdutos: function(){
			const vm = this;
			axios.get("/rs/fabricantes")
			.then(response => {
				vm.listaProdutos = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
    	
    	onSubmit(evt) {
            evt.preventDefault()
            alert(JSON.stringify(this.form))
    },
    onReset: function(evt){
    	evt.preventDefault()
        // Reset our form values
        this.form.email = ''
        this.form.name = ''
        this.form.food = null
        this.form.checked = []
        // Trick to reset/clear native browser form validation state
        this.show = false
        this.$nextTick(() => {
          this.show = true
        })
    }
    }
	
});
