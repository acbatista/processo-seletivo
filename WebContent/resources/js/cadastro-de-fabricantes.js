var inicio = new Vue({
	el:"#cadastro-fabricante",
    data: {
    	fabricante:{},
    	fabricante: {
            id: "",
            nome: ""
          },
    	listaFabricante:{}
    },
    created: function(){
        let vm =  this;
        vm.listar();
    },
    methods:{
    	criar:function(){
			const vm = this;
			if(!vm.fabricante.id){
				axios.post("/rs/fabricantes", vm.fabricante).then(response => {
					alert('Fabricante cadastrado com sucesso.');
					vm.listar();
				}).catch(function (error) {
					alert(error);
				}).finally(function() {});
			}else{
				axios.put("/rs/fabricantes/"+vm.fabricante.id, vm.fabricante).then(response => {
					alert('Fabricante atualizado com sucesso.');
					vm.listar();
				}).catch(function (error) {
					alert(error);
				}).finally(function() {});
			}
		},
		listar: function(){
			const vm = this;
			axios.get("/rs/fabricantes")
			.then(response =>{
				vm.listaFabricante = response.data;
				console.log(response);
			}).catch(function(error){
				alert(errore);
			})
		},
		editar: function(fabricante){
			const vm = this;
			this.fabricante = fabricante;
		},
		remover: function(id){
			const vm = this;
			if(confirm("Você realmente deseja excluir")){
				axios.delete("/rs/fabricantes/" + id)
				.then(response =>{
					vm.listar();
					console.log(response)
				}).catch(function(error){
					console.log(error)
				})
			}
		}
    		
    }
	
});
