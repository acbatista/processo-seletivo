var inicio = new Vue({
	el:"#inicio",
    data: {
        listaProdutos: {},
        produto:{
        	nome: '',
        	volume: '',
        	unidade: '',
        	estoque: '',
        	fabricante: {},
        },
        listaFabricantes: {},
        fabricanteSelecionado: null
    },
    created: function(){
        let vm =  this;
        vm.buscaProdutos();
        vm.buscaFabricante();
    },
    methods:{
    	 buscaProdutos: function(){
 			const vm = this;
 			axios.get("/rs/produtos")
 			.then(response => {
 				vm.listaProdutos = response.data;
 			}).catch(function (error) {
 				console.log(error);
 			}).finally(function() {
 			});
 		},
		buscaFabricante: function(){
			const vm = this;
			axios.get("/rs/fabricantes")
			.then(response =>{
				vm.listaFabricantes = response.data;
			}).catch(function (error){
				console.log(error);
			})
		},
		criar: function(data){
			const vm = this;
			if(!vm.produto.id){
				vm.produto = {
						nome: data.nome,
						volume: data.volume,
						unidade: data.unidade,
						estoque: data.estoque
				},
				vm.listaFabricantes.forEach(f => {
					if(f.nome == vm.fabricanteSelecionado){
						vm.produto.fabricante = f;
					}
				})
				
				axios.post("/rs/produtos", vm.produto).then(response => {
					alert('Fabricante cadastrado com sucesso.');
					vm.buscaProdutos();
			        vm.buscaFabricante();
				}).catch(function (error) {
					alert(error);
				}).finally(function() {});
			}else{
				vm.produto = {
						id: data.id,
						nome: data.nome,
						volume: data.volume,
						unidade: data.unidade,
						estoque: data.estoque
				},
				vm.listaFabricantes.forEach(f => {
					if(f.nome == vm.fabricanteSelecionado){
						vm.produto.fabricante = f;
					}
				})
				axios.put("/rs/produtos/"+vm.produto.id, vm.produto).then(response => {
					alert('Produto atualizado com sucesso.');
					vm.buscaProdutos();
			        vm.buscaFabricante();
				}).catch(function (error) {
					alert(error);
				}).finally(function() {});
			}
			
			
		},
		editar: function(data){
			const vm = this;
			vm.produto = data;
		},
		remover: function(id){
			const vm = this;
			if(confirm("Você realmente deseja excluir")){
				axios.delete("/rs/produtos/" + id)
				.then(response =>{
					vm.buscaProdutos();
			        vm.buscaFabricante();
					console.log(response)
				}).catch(function(error){
					console.log(error)
				})
			}
			
		}
 		
    }
});